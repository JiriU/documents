# Prohlášení

platí pro případ zvolení do níže uvedené funkce

- jméno a příjmení: Václav Klecanda
- místo narození: Tábor
- rodné číslo: 830927/1718
- trvale bytem: Chýnovská 127, Tábor
- organizace: Otevřená města, z. s.
- název funkce: člen projektové rady

Prohlašuji, že jsem způsobilý vykonávat výše uvedenou funkci, zejména prohlašuji, 
že nejsem v úpadku a jsem plně svéprávný. 
Tuto funkci přijímám a souhlasím se zápisem mé osoby do veřejného rejstříku.
Zavazuji se řídit stanovami organizace. 
Souhlasím s trvalým zveřejněním přiložených dokumentů na internetových stránkách organizace.

V Táboře dne 18.6. 2016