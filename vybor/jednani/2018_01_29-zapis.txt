﻿2018-01-29
přítomni: Marcel, Zbynek, Láďa, Dan
agenda:
1. hlasování per rollam
2. stav úkolů
    • Zveřejnění zápisu členské schůze
    • Zápis změny obsazení výboru do restříku
    • Proplacení faktury Internet Streamu
    • Marcel zavolá panu Vyhnánkovi, do konce týdne
3. Stav CityVizoru
    • Praha 5: Radomír?
4. rozpočet
    • členové pošlou Marcelovi podněty k projektům, včetně finančních nároků
5. Nový web Praha 7 (ten oss na wordpressu): https://novy.praha7.cz/
    • Ondra zkusí projednat
6. členská schůze
    • 28.5. Praha, Ondra zjistí, zda je možné sehnat místnost v PSP
7. projektová rada
    • Členové výboru do příští schůze připraví návrhy kandidátů
8. IoT projekt | Lora | base48
9. dotační tituly (dotaz na Radomíra)
    • Radomír zjistí do příští schůze, Dan připomene
10. SLA
    • Marcel pročte
11. Datovka - Dan
13. IM členů výboru
    • Vyzkoušíme RIOT - https://about.riot.im/
14. konference INSPIRUJME SE mesty a regiony | 2018-02-28 až 28 | přihláška příspěvků do 2018-01-29 | http://www.inspirujmese.cz/cs/content/informace-pro-prednasejici-0
    • cena 800 když předem převodem (zda platí i přednášející nevím)
15. Brno dělalo spolupráci na systému Česká obec (připsáno, aby nezapadlo). Prý používá Brno-jih a -sever | http://www.ceskaobec.cz/
16. odstraňování ilegálního obsahu v on-line prostředí | http://eur-lex.europa.eu/legal-content/CS-EN/TXT/?uri=CELEX:52017DC0555&from=EN
17. Ondřej
    • Open data expo
    • Asociace pro elektromobilitu




zápis:
1. hlasování per rollam
2. stav úkolů
    • Zveřejnění zápisu členské schůze
    • Zápis změny obsazení výboru do restříku
    • Proplacení faktury Internet Streamu
    • nový úkol: Dan napíše vzor PDF, zkusí poslat
3. Stav CityVizoru
    • Praha 5: Dan dá vědět ohledně IS
4. rozpočet
    • členové pošlou Marcelovi podněty k projektům, včetně finančních nároků
5. Nový web Praha 7 (ten oss na wordpressu): https://novy.praha7.cz/
    • Ondra zkusí projednat
6. členská schůze
    • 28.5. Praha, Ondra zjistí, zda je možné sehnat místnost v PSP
7. projektová rada
    • Členové výboru do příští schůze připraví návrhy kandidátů
8. IoT projekt | Lora | base48
    • AI: Lada zpracuje jako projekt
9. SLA
    • Marcel pročte
10. IM členů výboru
    • Vyzkoušíme RIOT - https://about.riot.im/
11. konference INSPIRUJME SE mesty a regiony | 2018-02-28 až 28 | přihláška příspěvků do 2018-01-29 | http://www.inspirujmese.cz/cs/content/informace-pro-prednasejici-0
    • cena 800 když předem převodem (zda platí i přednášející nevím)
12. Brno dělalo spolupráci na systému Česká obec (připsáno, aby nezapadlo). Prý používá Brno-jih a -sever | http://www.ceskaobec.cz/
    • Není naše priorita
13. odstraňování ilegálního obsahu v on-line prostředí | http://eur-lex.europa.eu/legal-content/CS-EN/TXT/?uri=CELEX:52017DC0555&from=EN
    • Nebudeme se tomuto tématu věnovat
14. Ondřej
    • Open data expo
15. Asociace pro elektromobilitu
    • AI: Marcel zkontaktuje, zjisti transparentnost


