agenda:
1. hlasování per rollam
* public money => public code
2. stav úkolů
* výzvy k zaplacení členských příspěvků do 31.8.2017
o Laďa vystaví odeslané datové zprávy v gitu
* Marcel domluví s účetním použití tagů ve FlexiBee
* public money, public code
o hlasování per rollam
* Marcel zřídí přístup Danovi do datové schránky
* Laďa zveřejní zápis z členské schůze
* zápis změny obsazení výboru do restříku
o připraví Dan, jakmile mu Marcel zřídí přístup do datovky
* Konto Bariéry: žádost o členství
o Laďa promluví s p. Hazuzou
* Zbyněk do konce října předloží návrh nové funkcionality evidence smluv BISON, kterou dále budeme implementovat pomocí modulů
* newsletter: Dan vytvoří draft do 29.10.
* Laďa nastaví alias cityvizor@otevrenemesta.cz na Dana
* aktualizace dat Nového Města na Moravě pro CityVizor: Ondřej pořeší
* open data - Brno (SLA) | katalog městských dat  https://kmd.otevrenamesta.cz/ a katalog otevřených dat https://ckan.otevrenamesta.cz/
o https://monitor.otevrenamesta.cz/  https://diskurz.otevrenamesta.cz/
o Laďa probere s Brnem
3. žádost o členství: Asociace pro elektromobilitu
4. Cityvizor
5. vykazování účasti na jednání výboru
6. ostatní

zápis:
1. hlasování per rollam
* public money => public code
o "Výbor pověřuje Marcela Kolaju podpisem otevřeného dopisu Public Money, Public Code jménem spolku:
o https://publiccode.eu/openletter/"
o Usnesení bylo přijato:
o pro: Kolaja, Nešněra, Profant, Grepl, Palovský
o proti: nikdo
o zdržel se: nikdo
o AI: Marcel provede
2. stav úkolů
* výzvy k zaplacení členských příspěvků do 31.8.2017
o Laďa vystaví odeslané datové zprávy v gitu do 8.11.
* Marcel domluví s účetním použití tagů ve FlexiBee
o domluveno, přikročíme k implementaci
* public money, public code
o hlasování per rollam
o hotovo
* Marcel zřídí přístup Danovi do datové schránky
o bude zřízeno do 12.11.
* Laďa zveřejní zápis z členské schůze
o bude zveřejněno, do 9.11.
* zápis změny obsazení výboru do restříku
o připraví Dan, jakmile mu Marcel zřídí přístup do datovky
* Konto Bariéry: žádost o členství
o Laďa promluví s p. Hazuzou
o Láďa zavolá příští týden
* Zbyněk do konce října předloží návrh nové funkcionality evidence smluv BISON, kterou dále budeme implementovat pomocí modulů
o Dan zaurguje
* newsletter: Dan vytvoří draft do 29.10.
o Dan vytvoří druhou verzi, do konce týdne dodá výbor připomínky a v pondělí bude rozesláno
* Laďa nastaví alias cityvizor@otevrenemesta.cz na Dana
o hotovo
* aktualizace dat Nového Města na Moravě pro CityVizor: Ondřej pořeší
o zkusí Dan stáhnout z NMNM a přeházet do požadovaného formátu. Pak to pošle Martinovi Kopečkovi
o Láďa pošle odkaz na pumpu - ukázka proběhla živě - OK
o Ondřej sepíše poptávku po programátorovi do Konference
* open data - Brno (SLA) | katalog městských dat  https://kmd.otevrenamesta.cz/ a katalog otevřených dat https://ckan.otevrenamesta.cz/
o https://monitor.otevrenamesta.cz/  https://diskurz.otevrenamesta.cz/
o Laďa probere s Brnem
3. žádost o členství: Asociace pro elektromobilitu
o Marcel jim napíše mail, co od členství očekávají a jak mohou přispět k činnosti OM
4. Cityvizor
o Dan v redminu specifikuje, co vše je potřeba. Laďa
o http://ukoly.openalt.org/projects/rozklikavaci_rozpocet - změněn název projektu - OK
o https://ukoly.openalt.org/issues/4077 - OK
5. vykazování účasti na jednání výboru
o Dan smaže podúkol, nyní bude všechna účast zde: http://ukoly.openalt.org/issues/4078
6. ostatní

